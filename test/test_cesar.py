import unittest

from cesar import chiffrer
from cesar import dechiffrer

class TestMathsFunctions(unittest.TestCase):

    def test_chiffrer(self):
        self.assertEqual(chiffrer("Bonjour"), "Jwvrwcz")
    def test_dechiffrer(self):
        self.assertEqual(dechiffrer("Jwvrwcz"), "Bonjour")